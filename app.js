const date = new Date();
const months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
                "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
const max_colors = 7;
const random_colors = ["#F0F", "#14adbd", "#21fc0b", "#fc250b", "#0b1dfc", "fcea0b", "0bfcea"];


//funcion randomInt extraida de 
//https://dirask.com/posts/JavaScript-Math-random-method-example-x1R6G1?gclid=Cj0KCQiAhs79BRD0ARIsAC6XpaV2n2ZZwjEsu2Aa27hp9ENJpFKWJZR5zWM-zhPLXiudtfqp0zwo9fMaAkcqEALw_wcB&utm_source=google_search&utm_medium=cpc&utm_campaign=js_math&gclid=Cj0KCQiAhs79BRD0ARIsAC6XpaV2n2ZZwjEsu2Aa27hp9ENJpFKWJZR5zWM-zhPLXiudtfqp0zwo9fMaAkcqEALw_wcB
function randomInt(min, max) {
    return min + Math.floor((max - min) * Math.random());
}

function consumeHolidays() {
    return new Promise(function (resolve, reject) {
        var holidays = new Array();
        for (let i = 0; i < 12; i++) {
            holidays.push([]);
        }

        var request = new XMLHttpRequest();

        request.open('GET', 'http://nolaborables.com.ar/api/v2/feriados/' + date.getFullYear(), true);
        request.onload = function () {

          var data = JSON.parse(this.response);

          if (request.status >= 200 && request.status < 400) {
            data.forEach((obj) => {
              holidays[obj.mes - 1].push(obj.dia);
            })

            resolve(holidays);
          } else {
            console.log('Error');
            reject(request.status);
          }
        }
        request.send();
    })
}
    
async function renderCalendar() {
    var holidays = await consumeHolidays();

    document.getElementById('month').innerHTML = months[date.getMonth()] + ', ' + date.getFullYear();
    lastDay = new Date(date.getFullYear(), date.getMonth(), 1);

    const fristDayMonth = new Date(date.getFullYear(), date.getMonth(), 1);
    const lastDayMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    let days = '';

    for(let i = 0; i < fristDayMonth.getDay(); i++) {
        days += '<div></div>';
    }
    
    for(let i = 1; i <= lastDayMonth.getDate(); i++) {

        if(i == new Date().getDate() && date.getMonth() == new Date().getMonth() 
            && date.getFullYear() == new Date().getFullYear()) {
            days += '<div class="today">' + i + '</div>';
        } else  if(holidays[date.getMonth()].includes(i)){
            days += '<div class="holiday">' + i + '</div>';
        } else {
            days += '<div>' + i + '</div>';
        }
    }
    document.getElementById('calendays').innerHTML = days;
}

function changeMonthColor() {
    var monthBox = document.getElementById("month-box");
    monthBox.style.transition = "background 1s linear 0s";
    monthBox.style.background = random_colors[randomInt(0, max_colors)];
}

function previousMonth() {
    changeMonthColor();
    date.setMonth(date.getMonth() - 1);
    renderCalendar();
}

function nextMonth() {
    changeMonthColor();
    date.setMonth(date.getMonth() + 1);
    renderCalendar();
}


renderCalendar();